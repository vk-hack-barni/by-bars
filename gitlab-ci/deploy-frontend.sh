#!/usr/bin/env bash

USAGE="Usage: ./deploy.sh"

case "$BRANCH" in
    master)
      PORT=8090;;
    dev)
      PORT=8091;;
    *)
      echo "$USAGE"
      exit 1;;
esac

IMAGE="registry.gitlab.com/vk-hack-barni/by-bars:$BRANCH"
NAME="bybars-frontend-$BRANCH"
docker pull "$IMAGE"
docker stop "$NAME" && docker rm "$NAME" || true
docker run -d --restart=unless-stopped --name="$NAME" -p "$PORT":80 "$IMAGE"
