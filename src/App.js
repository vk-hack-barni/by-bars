import React from 'react';
import connect from 'storeon/react/connect'
import '@vkontakte/vkui/dist/vkui.css';
import {
    View,
    Panel,
    PanelHeader,
    TabbarItem,
    Tabbar,
    Epic,
    ModalRoot,
    ModalPage,
    ModalPageHeader,
    IS_PLATFORM_ANDROID,
    HeaderButton,
    IS_PLATFORM_IOS,
    Gallery,
    Group,
    List,
    Cell,
    InfoRow,
    Div
} from '@vkontakte/vkui';

import bar from './img/bar.png';
import geo from './img/geo.png';
import point from './img/point.png';

import Icon28Place from '@vkontakte/icons/dist/28/place';
import Icon28Game from '@vkontakte/icons/dist/28/game';
import Icon28UsersOutline from '@vkontakte/icons/dist/28/users_outline';
import Icon28HelpOutline from '@vkontakte/icons/dist/28/help_outline';
import Icon24Cancel from '@vkontakte/icons/dist/24/cancel';
import Icon24Dismiss from '@vkontakte/icons/dist/24/dismiss';
import Icon28Story from '@vkontakte/icons/dist/28/story';

import Teams from "./panels/teams/Teams";
import MapPage from "./panels/map/MapPage";
import QuestPage from "./panels/quest/QuestPage";
import CellButton from "@vkontakte/vkui/dist/components/CellButton/CellButton";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import QuestInfo from "./panels/quest/QuestInfo";
import Stories from "./panels/stories/Stories";

class App extends React.Component {

    onClickPanelChange = (e) => {
        this.props.dispatch('onStoryChange', e.currentTarget.dataset.story)
    };
    addTeamMember = (item) => {
        this.props.dispatch('addTeamMember', {id: item.id, teamId: this.props.userData.id, teamName: "test"});
    };

    componentDidMount() {
        this.props.dispatch('init');
    }

    render() {
        let activeStory = this.props.activeStory;
        let selectBarInfo = this.props.selectBarInfo;
        let selectPointInfo = this.props.selectPointInfo;
        let friends = this.props.friends;

        let modal = (
            <ModalRoot activeModal={this.props.activeModal}>
                <ModalPage
                    id="barInfo"
                    header={
                        <ModalPageHeader
                            left={IS_PLATFORM_ANDROID &&
                            <HeaderButton
                                onClick={() => this.props.dispatch('modal/null')}><Icon24Cancel/></HeaderButton>}
                            right={IS_PLATFORM_IOS &&
                            <HeaderButton
                                onClick={() => this.props.dispatch('modal/null')}><Icon24Dismiss/></HeaderButton>}
                        >
                            {selectBarInfo.name}
                        </ModalPageHeader>
                    }
                    onClose={() => this.props.dispatch('modal/null')}
                    settlingHeight={80}
                >
                    {!!selectBarInfo.id ?
                        <div>
                            <Gallery
                                slideWidth="custom"
                                style={{height: 200}}
                            >
                                {selectBarInfo.photos.map((url, i) => (
                                    <img style={{width: "auto"}} key={i} src={url} alt=""/>))}
                            </Gallery>
                            <Group title="Информация о баре">
                                <List>
                                    <Cell>
                                        <InfoRow title="Адрес">
                                            {selectBarInfo.address}
                                        </InfoRow>
                                    </Cell>
                                    <Cell>
                                        <InfoRow title="Время работы">
                                            {selectBarInfo.worktime}
                                        </InfoRow>
                                    </Cell>
                                    <Cell>
                                        <InfoRow title="Оценка">
                                            {selectBarInfo.rating}
                                        </InfoRow>
                                    </Cell>
                                </List>
                            </Group>
                            <Group>
                                <Div>
                                    <InfoRow title="Описание">
                                        {selectBarInfo.description}
                                    </InfoRow>
                                </Div>
                            </Group>
                        </div>
                        : null
                    }
                </ModalPage>
                <ModalPage
                    id="pointInfo"
                    header={
                        <ModalPageHeader
                            left={IS_PLATFORM_ANDROID &&
                            <HeaderButton
                                onClick={() => this.props.dispatch('modal/null')}><Icon24Cancel/></HeaderButton>}
                            right={IS_PLATFORM_IOS &&
                            <HeaderButton
                                onClick={() => this.props.dispatch('modal/null')}><Icon24Dismiss/></HeaderButton>}
                        >
                            {selectPointInfo.name}
                        </ModalPageHeader>
                    }
                    onClose={() => this.props.dispatch('modal/null')}
                    settlingHeight={80}
                >
                    {!!selectPointInfo.id ?
                        <div>
                            <Gallery
                                slideWidth="custom"
                                style={{height: 200}}
                            >
                                {selectPointInfo.photos.map((url, i) => (
                                    <img style={{width: "auto"}} key={i} src={url} alt=""/>))}
                            </Gallery>
                            {this.props.selectQuestId > 0 && <Group title="Что делать?">
                                <List>
                                    <Cell>
                                        <InfoRow>
                                            {selectPointInfo.task}
                                        </InfoRow>
                                    </Cell>
                                </List>
                            </Group>}
                            <Group title="Информация о баре">
                                <List>
                                    <Cell>
                                        <InfoRow title="Адрес">
                                            {selectPointInfo.address}
                                        </InfoRow>
                                    </Cell>
                                    <Cell>
                                        <InfoRow title="Время работы">
                                            {selectPointInfo.worktime}
                                        </InfoRow>
                                    </Cell>
                                    <Cell>
                                        <InfoRow title="Оценка">
                                            {selectPointInfo.rating}
                                        </InfoRow>
                                    </Cell>
                                </List>
                            </Group>
                            <Group>
                                <Div>
                                    <InfoRow title="Описание">
                                        {selectPointInfo.description}
                                    </InfoRow>
                                </Div>
                            </Group>
                        </div>
                        : null
                    }
                </ModalPage>

                <ModalPage
                    id="addTeam"
                    header={
                        <ModalPageHeader
                            left={IS_PLATFORM_ANDROID &&
                            <HeaderButton
                                onClick={() => this.props.dispatch('modal/null')}><Icon24Cancel/></HeaderButton>}
                            right={IS_PLATFORM_IOS &&
                            <HeaderButton
                                onClick={() => this.props.dispatch('modal/null')}><Icon24Dismiss/></HeaderButton>}
                        >
                            Добавить команду
                        </ModalPageHeader>
                    }
                    onClose={() => this.props.dispatch('modal/null')}
                    settlingHeight={80}
                >
                    <div>
                        {!!friends &&
                        <Group>
                            {
                                friends.map((item, i) => (
                                    <CellButton
                                        key={i} before={item.photo_200 ? <Avatar src={item.photo_200}/> : null}
                                        description={item.city && item.city.title ? item.city.title : ''}
                                        onClick={() => this.addTeamMember(item)}
                                    >
                                        {`${item.first_name} ${item.last_name}`}
                                    </CellButton>
                                ))
                            }
                        </Group>}

                    </div>
                </ModalPage>

            </ModalRoot>
        );

        return (
            <Epic id="main" activeStory={activeStory} tabbar={
                <Tabbar>
                    <TabbarItem
                        onClick={this.onClickPanelChange}
                        selected={activeStory === 'map'}
                        data-story="map"
                        text="Карта"
                    ><Icon28Place/></TabbarItem>
                    <TabbarItem
                        onClick={this.onClickPanelChange}
                        selected={activeStory === 'quest'}
                        data-story="quest"
                        text="Квест"
                    ><Icon28Game/></TabbarItem>
                    <TabbarItem
                        onClick={this.onClickPanelChange}
                        selected={activeStory === 'teams'}
                        data-story="teams"
                        text="Команды"
                    ><Icon28UsersOutline/></TabbarItem>
                    <TabbarItem
                        onClick={this.onClickPanelChange}
                        selected={activeStory === 'help'}
                        data-story="help"
                        text="Помощь"
                    ><Icon28HelpOutline/></TabbarItem>
                    <TabbarItem
                        onClick={this.onClickPanelChange}
                        selected={activeStory === 'stories'}
                        data-story="stories"
                        text="Сторис"
                    ><Icon28Story/></TabbarItem>
                </Tabbar>
            }>
                <View id="map" modal={modal} popout={this.props.popout} activePanel="map">
                    <Panel id="map">
                        <MapPage/>
                    </Panel>
                </View>
                <View id="quest" modal={modal} popout={this.props.popout}
                      activePanel={this.props.selectQuestId > 0 ? "questInfo" : "quest"}>
                    <Panel id="quest">
                        <QuestPage/>
                    </Panel>
                    <Panel id="questInfo">
                        <QuestInfo/>
                    </Panel>
                </View>
                <View id="teams" modal={modal} popout={this.props.popout} activePanel="teams">
                    <Panel id="teams">
                        <Teams/>
                    </Panel>
                </View>
                <View id="help" modal={modal} popout={this.props.popout} activePanel="help">
                    <Panel id="help">
                        <PanelHeader>Помощь</PanelHeader>
                        <Group title="Условные обозначения">
                            <List>
                                <Cell indicator={<img src={bar} alt=""/>}>Бары</Cell>
                                <Cell indicator={<img src={point} alt=""/>}>Достопримечательности</Cell>
                                <Cell indicator={<img src={geo} alt=""/>}>Моё местоположение</Cell>
                            </List>
                        </Group>
                        <Group title="О нас">
                            <List>
                                <Cell>
                                    <InfoRow title="Frontend">
                                        <a href="https://vk.com/kirgurkir">Кирилл Гурлев</a>
                                    </InfoRow>
                                </Cell>
                                <Cell>
                                    <InfoRow title="Frontend">
                                        <a href="https://vk.com/marrum">Мари Румянцева</a>
                                    </InfoRow>
                                </Cell>
                                <Cell>
                                    <InfoRow title="DevOps/Backend">
                                        <a href="https://vk.com/basa62">Артем  Басалаев</a>
                                    </InfoRow>
                                </Cell>
                                <Cell>
                                    <InfoRow title="Backend">
                                        <a href="https://vk.com/id182032859">Николай Дурникин</a>
                                    </InfoRow>
                                </Cell>
                            </List>
                        </Group>
                        <Group title="Git проекта">
                            <List>
                                <Cell>
                                    <InfoRow>
                                        <a href="https://gitlab.com/vk-hack-barni/by-bars">GitLab</a>
                                    </InfoRow>
                                </Cell>
                            </List>
                        </Group>
                    </Panel>
                </View>
                <View id="stories" activePanel="main">
                    <Panel id="main">
                        <Stories/>
                    </Panel>
                </View>
            </Epic>
        );
    }
}


export default connect('selectQuestId', 'activeStory', 'popout', 'activeModal', 'selectBarInfo', 'selectPointInfo', 'friends', 'userData', 'token', App)

