import React from 'react';
import connect from 'storeon/react/connect'
import '@vkontakte/vkui/dist/vkui.css';
import {Group, PanelHeader, File, FormLayout, Button} from "@vkontakte/vkui";
import watermark from "watermarkjs/lib";
import small from '../../img/small.png';

import Icon24Camera from '@vkontakte/icons/dist/24/camera';

class Stories extends React.Component {
    state = {
        uploading: false,
        images: {},
        newImg: ''
    };

    onChange = e => {
        let inputImg = e.target.files[0];
        console.log("inputImg: ", inputImg);

        const upload = e.target.files[0];
        watermark([upload, small])
            .image(watermark.image.lowerRight(0.5))
            .then(img => {
                this.setState({...this.state, newImg: img.src});
            })
            //.then(img => console.log(img))
            .catch(err => console.log("err: ", err));

    };

    render() {
        let newImg = this.state.newImg;
        return (
            <div>
                <PanelHeader>Stories</PanelHeader>
                <Group>
                    <FormLayout>
                        <File onChange={this.onChange} top="Загрузите ваше фото" before={<Icon24Camera/>} size="l">
                            Открыть галерею
                        </File>
                    </FormLayout>
                </Group>
                <Group>
                    {!!this.state.newImg &&
                    <FormLayout>
                        <img style={{width: '100%'}} src={newImg} alt=""/>
                        <Button size="xl" level="secondary" onClick={this.props.dispatch('stories/getServer', this.state.newImg)}>Выложить историю</Button>
                    </FormLayout>
                    }
                </Group>
            </div>
        )
    }
}

export default connect(Stories)
