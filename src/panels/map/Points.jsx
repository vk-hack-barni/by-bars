import connect from "storeon/react/connect";
import React from "react";
import {Clusterer, Placemark, Polyline} from "react-yandex-maps";


class Points extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectBarId: null,
            modalHistory: []
        };

        this.modalBack = () => {
            this.setActiveModal(this.state.modalHistory[this.state.modalHistory.length - 2]);
        };
    }


    render() {
        if (this.props.mapState !== 'quest') {
            return (
                <div>
                    {(this.props.mapState === 'all' || this.props.mapState === 'onlyBars') &&
                        <Clusterer
                            options={{preset: 'islands#invertedDarkOrangeClusterIcons', groupByCoordinates: false}}>
                            {this.props.barList.map((data) => (
                                <Placemark key={data.id} geometry={[data.location.lat, data.location.lon]}
                                           options={{preset: 'islands#orangeStretchyIcon'}}
                                           properties={{iconContent: data.name.substr(0, 15)}}
                                           onClick={() => {
                                               this.props.dispatch('getBar', data.id);
                                           }}
                                />
                            ))}
                        </Clusterer>}
                    {(this.props.mapState === 'all' || this.props.mapState === 'onlyPlaces') &&
                        <Clusterer
                            options={{preset: 'islands#invertedLightBlueClusterIcons', groupByCoordinates: false}}>
                            {this.props.pointList.map((data) => (
                                <Placemark key={data.id} geometry={[data.location.lat, data.location.lon]}
                                           options={{preset: 'islands#lightBlueStretchyIcon'}}
                                           properties={{iconContent: data.name.substr(0, 15)}}
                                           onClick={() => {
                                               this.props.dispatch('getPoint', data.id);
                                           }}
                                />
                            ))}
                        </Clusterer>}
                    {this.props.position.available > 0 &&
                        <Placemark geometry={[this.props.position.lat, this.props.position.long]}
                                   options={{preset: 'islands#redCircleDotIcon'}}/>
                    }
                </div>
            )
        } else {
            let points = [];
            let bar = [];
            let line = [];
            this.props.selectQuestInfo.route.forEach((data)=>{
                line.push([data.location.lat, data.location.lon]);
                if (data.type === "bar") bar.push(data);
                if (data.type === "point") points.push(data);
            });
            return (
                <div>
                    {bar.map((data) => (
                        <Placemark key={data.id} geometry={[data.location.lat, data.location.lon]}
                                   options={{preset: 'islands#orangeStretchyIcon'}}
                                   properties={{iconContent: data.name.substr(0, 15)}}
                                   onClick={() => {
                                       this.props.dispatch('getBar', data.id);
                                   }}
                        />
                    ))}

                    {points.map((data) => (
                        <Placemark key={data.id} geometry={[data.location.lat, data.location.lon]}
                                   options={{preset: 'islands#lightBlueStretchyIcon'}}
                                   properties={{iconContent: data.name.substr(0, 15)}}
                                   onClick={() => {
                                       this.props.dispatch('getPoint', data.id);
                                   }}
                        />
                    ))}
                    {this.props.position.available &&
                    <Placemark geometry={[this.props.position.lat, this.props.position.long]}
                               options={{preset: 'islands#redCircleDotIcon'}}/>
                    }

                    <Polyline
                        geometry={line}
                        options={{
                            balloonCloseButton: false,
                            strokeColor: '#000',
                            strokeWidth: 4,
                            strokeOpacity: 0.5,
                        }}
                    />
                </div>

            )
        }

    }
}

export default connect('position', 'selectQuestInfo','mapState', 'barList', 'pointList', Points)