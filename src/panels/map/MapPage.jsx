import {PanelHeader, Select} from "@vkontakte/vkui";
import connect from "storeon/react/connect";
import React from "react";
import {Map, YMaps, Polyline} from "react-yandex-maps";

import Points from "./Points";

class MapPage extends React.Component {

    hendlerMapChangeBounds(e) {
        this.props.dispatch('map/changeBounds', e.originalEvent.newBounds);
    }

    render() {
        let points = [
            [55.684758, 37.738521],
            [55.685758, 37.738521],
            [55.684758, 37.739721]
        ];

        return (
            <div style={{height: "631px"}}>
                <PanelHeader>По барам!</PanelHeader>
                <Select value={this.props.mapState}
                        onChange={(e) => this.props.dispatch('map/setState', e.target.value)}>
                    <option value="all">Вся карта</option>
                    <option value="onlyBars">Только бары</option>
                    <option value="onlyPlaces">Только места</option>
                    <option value="quest">Квест</option>
                </Select>
                <YMaps>
                    <Map width={'auto'} height={'100%'} defaultState={{center: [this.props.position.lat, this.props.position.long], zoom: 13}}
                         instanceRef={(inst) => !!inst ? inst.events.add('boundschange', (e) => this.hendlerMapChangeBounds(e)) : null}>
                        <Points />
                        {this.props.mapState === "quest" ?
                            <Polyline
                                geometry={points}
                                options={{
                                    balloonCloseButton: false,
                                    strokeColor: '#000',
                                    strokeWidth: 4,
                                    strokeOpacity: 0.5,
                                }}
                            /> : null}
                    </Map>
                </YMaps>
            </div>
        )
    }
}

export default connect( 'position', 'mapState', MapPage)