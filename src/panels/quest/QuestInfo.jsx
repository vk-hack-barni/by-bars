import React from 'react';
import connect from 'storeon/react/connect'
import '@vkontakte/vkui/dist/vkui.css';
import {PanelHeader, Avatar, Cell, Group, Button, PanelHeaderBack} from '@vkontakte/vkui';

class QuestInfo extends React.Component {

    render() {
        let selectQuestInfo = this.props.selectQuestInfo;

        return (
            <div>
                <PanelHeader left={<PanelHeaderBack onClick={() => this.props.dispatch('quest/null')}/>}>
                    {selectQuestInfo.name ? selectQuestInfo.name : "Квест"}
                </PanelHeader>
                {!!selectQuestInfo.id ?
                    <div>
                        <Group>
                            <Cell
                                photo={selectQuestInfo.logo}
                                description={selectQuestInfo.location}
                                bottomContent={<div><Button
                                    onClick={() => this.props.dispatch('quest/Done')}>Готово</Button><Button
                                    onClick={() => this.props.dispatch('quest/seeMap')} style={{marginLeft: '10px'}}>См
                                    карту</Button>
                                </div>}
                                before={<Avatar src={selectQuestInfo.logo} size={80}/>}
                                size="l"
                            >
                                {selectQuestInfo.name}
                            </Cell>
                        </Group>
                        <p style={{color: '#909499', marginLeft: '10px', marginRight: '10px', fontSize: '15px'}}>
                            Вперёд, приключения ждут! Когда закончишь жми кнопку "Готово".
                        </p>
                        <Group>
                            {selectQuestInfo.route.map((place) => {
                                return (
                                    <Cell
                                        style={{backgroundColor: place.type === "bar" ? "rgba(255,140,32,0.31)" : "rgba(130,205,255,0.31)"}}
                                        key={place.id}
                                        size="l"
                                        description={place.address}
                                        before={<Avatar src={place.photo}/>}
                                        asideContent={<Button onClick={()=>{
                                            place.type === "bar" ? this.props.dispatch('getBar', place.id) : this.props.dispatch('getPoint', place.id);
                                        }}>{place.type === "point" ? 'Что делать?' : 'Подробнее'}</Button>}
                                    >
                                        {place.name}
                                        <p style={{
                                            marginTop: 0,
                                            marginBottom: 0,
                                            color: '#909499',
                                            fontSize: '12px'
                                        }}>{place.worktime}</p>
                                    </Cell>
                                )
                            })}
                        </Group>
                    </div> : <Group title="Загрузка..."/>
                }
            </div>
        );
    }
}


export default connect('selectQuestInfo', QuestInfo)

