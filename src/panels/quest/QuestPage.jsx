import React from 'react';
import connect from 'storeon/react/connect'
import '@vkontakte/vkui/dist/vkui.css';
import {PanelHeader, Avatar, Cell, Group, Button} from '@vkontakte/vkui';

class QuestPage extends React.Component {

    render() {
        let questList = this.props.questList;

        return (
            <div>
                <PanelHeader>Квесты</PanelHeader>
                {!!questList ?
                    <Group title="Выбери квест">
                        {questList.map((quest) => (
                            <Cell key={quest.id} photo={quest.logo}
                                description={quest.location}
                                bottomContent={<Button onClick={() => {
                                    this.props.dispatch('quest/selectQuest', quest.id);
                                    this.props.dispatch('quest/getQuestInfo');
                                }}>Поехали!</Button>}
                                before={<Avatar src={quest.logo} size={80}/>}
                                size="l"
                            >
                                {quest.name}
                            </Cell>
                        ))}
                    </Group> : <Group title="Пока что нет доступных квестов"/>
                }
            </div>
        );
    }
}


export default connect('questList', QuestPage)

