import React from 'react';
import connect from 'storeon/react/connect'
import '@vkontakte/vkui/dist/vkui.css';

import Cell from "@vkontakte/vkui/dist/components/Cell/Cell";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import CellButton from "@vkontakte/vkui/dist/components/CellButton/CellButton";
import Icon24Add from '@vkontakte/icons/dist/24/add';
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import Button from "@vkontakte/vkui/dist/components/Button/Button";

class Teams extends React.Component {
    addTeam = () => {
        this.props.dispatch('addTeam', this.props.userData.id);
        this.props.dispatch('modal/addTeam');
        this.props.dispatch('addTeamMember', {id: this.props.userData.id, teamId: this.props.userData.id, teamName: "test"});
    };

    createChat = () => {
        this.props.dispatch('createChat');
    };
    componentDidMount() {
        this.props.dispatch('getToken');
            this.props.dispatch('getFriendsInfo');
        this.props.dispatch('getAllUsersInfo');
        this.props.dispatch('setTeamNumber');
    }
    render() {
        let fetchedUser = this.props.userData;
        let users = this.props.users;
        let userInfo = this.props.userInfo;
        console.log(users);
        console.log(this.props);
        return (
            <div>
                <PanelHeader>Команды</PanelHeader>
                {fetchedUser &&
                <div>
                    <Group title="Моя команда">
                        <Cell
                            before={fetchedUser.photo_200 ? <Avatar src={fetchedUser.photo_200}/> : null}
                            description={fetchedUser.city && fetchedUser.city.title ? fetchedUser.city.title : ''}
                        >
                            {`${fetchedUser.first_name} ${fetchedUser.last_name}`}
                        </Cell>
                    </Group>
                    <Group title="Команда">
                        <CellButton onClick={this.addTeam} before={<Icon24Add/>}>Добавить участника в
                            команду</CellButton>
                    </Group>
                    <Div>
                        <Button onClick={this.createChat}>Поделиться приложением</Button>
                    </Div>
                </div>
                }
                <div>
                    {!!userInfo &&
                    <Group title="Рейтинг">

                        {

                            userInfo.map((item, i) => (
                                <Cell key={i}
                                      asideContent={item.score}
                                >
                                    {`${item.first_name} ${item.last_name}  `}
                                </Cell>)
                            )
                        }
                    </Group>}

                </div>
            </div>
        )
    }
}

export default connect('userData', 'friends', 'token', 'users', 'userInfo','teamNumber', Teams)
