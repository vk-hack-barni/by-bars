import connect from '@vkontakte/vk-connect';

export let User = store => {
    const apiServer = 'https://bybars-api.basa62.ru';
    // Initial state
    store.on('@init', () => ({
        sightList: [],
        id: 0,
        fetchUser: null,
        friends: [],
        userData: {},
        token: null,
        teamNumber: null,
        teamName: null,
        team: [],
        users: [],
        nowUser: null,
        userInfo: []
    }));

    store.on('getUserInfo', async () => {
        connect.send("VKWebAppGetUserInfo", {});
    });

    store.on('setUserInfo', ({userData}, newUserData) => {
        return {userData: newUserData}
    });

    store.on('getFriendsInfo', async ({token}) => {
        connect.send("VKWebAppCallAPIMethod", {
            "method": "friends.get", "params": {
                'count': 1000,
                'fields': 'city,domain,photo_100',
                'order': 'random',
                'v': '5.101',
                'access_token': token,
            }
        });
    });

    store.on('setFriends', ({friends}, newFriends) => {
        return {friends: newFriends}
    });

    store.on('getToken', () => {
        connect.send("VKWebAppGetAuthToken", {"app_id": 7150289, "scope": "friends,stories"})
    });

    store.on('setToken', ({token}, newToken) => {
        return {token: newToken}
    });

    store.on('addTeam', ({teamNumber},  data) => {
        return {teamNumber: data}

    });

    store.on('getUsers', async () => {
        let url = apiServer + '/get_all_users';
        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (newUsersList) {
                store.dispatch('setUsers', newUsersList);
            });
    });

    store.on('setTeamNumber', ({teamNumber, users, userData} ) => {
        console.log({teamNumber, users, userData} );
        let newTeamNumber = users.filter((user => user.id === userData.id))[0];
        if(newTeamNumber==null) {
            newTeamNumber=userData.id;
        }
        else {
            newTeamNumber=newTeamNumber.teamId;
        }
        return {teamNumber: newTeamNumber};
    });

    store.on('setUsers', ({users}, newUsersList) => {

        return {users: newUsersList};
    });

    store.on('addTeamMember', async ({team}, data) => {
        let url = apiServer + '/add_user';
        fetch(url, {
            method: 'POST', // или 'PUT'
            body: JSON.stringify(data), // data может быть типа `string` или {object}!
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => console.log('Успех:', JSON.stringify(response)))
            .catch(error => console.error('Ошибка:', error));
        return {team: team.concat([data])}
    });


    store.on('getAllUsersInfo', ({users, token, teamNumber}) => {
        console.log(teamNumber);
        let result = users.filter(user => user.teamId === teamNumber);
        console.log(result);
        let ids = new Array(result.length);
        result.forEach(function(item, i, result) {
            ids[i]=result[i].id;
        });

        connect.send("VKWebAppCallAPIMethod", {
            "method": "users.get", "params": {
                'user_ids': ids,
                'fields': 'city,domain,photo_50,status',
                'v': '5.101',
                'access_token': token,
            }
        });
    });
    store.on('setAllUsersInfo', ({userInfo, users}, newUsers) => {
        newUsers.forEach(function(item, i, newUsers) {
            newUsers[i].score=users.find(x => x.id === newUsers[i].id).score;
        });
        return {userInfo: newUsers};
    });
    store.on('createChat', () => {

        connect.send("VKWebAppShare", {});

    });

};
