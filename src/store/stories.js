import connect from '@vkontakte/vk-connect';

export let Stories = store => {
    const apiServer = 'https://bybars-api.basa62.ru';
    let uploadImg;

    store.on('stories/getServer', async ({token}, img) => {
        //store.dispatch('stories/setUploadImg', img);
        uploadImg = img;
        connect.send("VKWebAppCallAPIMethod", {
            "method": "stories.getPhotoUploadServer", "params": {
                'add_to_news': 0,
                'link_url': 'https://vk.com/app7150289',
                'v': '5.101',
                'access_token': token,
            }
        });
    });

    store.on('stories/setUploadImg', async ({uploadImg}, img) => {
        return {uploadImg: img};
    });

    store.on('stories/upload', ({token},uploadUrl) => {
        //console.log(uploadUrl);
        //console.log(uploadImg);

        // let fd = new FormData();
        // fd.append("image_data", uploadImg);

        let data = JSON.stringify({uploadImg: uploadImg, uploadUrl: uploadUrl});
        console.log(data);

        fetch(apiServer + '/send_story_image', {
            method: 'POST', // или 'PUT'
            body: data,
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => console.log('Успех:', JSON.stringify(response)))
            .catch(error => console.error('Ошибка:', error));
    });
};
