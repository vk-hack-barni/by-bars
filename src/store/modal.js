import {ScreenSpinner, Alert} from "@vkontakte/vkui";
import React from "react";

export let Modal = store => {

    // Initial state
    store.on('@init', () => ({
        activeModal: null,
        popout: null,
    }));

    store.on('popout/null', () => {
        return {popout: null}
    });
    store.on('popout/screenSpinner', () => {
        return {popout: <ScreenSpinner/>}
    });
    store.on('popout/error', ({popout}, error) => {
        return {
            popout: <Alert
                actionsLayout="vertical"
                actions={[{
                    title: 'Ок',
                    autoclose: true,
                    style: 'destructive'
                }]}
                onClose={() => store.dispatch('popout/null')}
            >
                <h2>Ошибка</h2>
                <p>Ошибка при загрузке. Проверьте соединение с интернетом.</p>
                <p>{error}</p>
            </Alert>
        }
    });
    store.on('popout/emptyQuest', ({popout}) => {
        return {
            popout: <Alert
                actionsLayout="vertical"
                actions={[{
                    title: 'Ок!',
                    autoclose: true,
                    style: 'cancel'
                }]}
                onClose={() => {store.dispatch('popout/null');store.dispatch('goToQuest')}}
            >
                <h2>Квест</h2>
                <p>Мы рады, что ты заглянул в этот раздел. Собери друзей, выбери квест и вперёд за новыми приключениями!</p>
            </Alert>
        }
    });

    store.on('popout/Done', ({popout}) => {
        return {
            popout: <Alert
                actionsLayout="vertical"
                actions={[{
                    title: 'Давай дальше',
                    autoclose: true,
                    style: 'cancel'
                }]}
                onClose={() => {store.dispatch('popout/null');store.dispatch('onStoryChange', 'stories')}}
            >
                <h2>Ты молодец!</h2>
                <p>Поделись с друзьямии своей прогулкой в stories.</p>
            </Alert>
        }
    });

    store.on('modal/null', ({activeModal}) => {
        if (activeModal !== null) {
            return {activeModal: null};
        }
    });
    store.on('modal/barInfo', ({activeModal}) => {
        return {activeModal: 'barInfo'};
    });
    store.on('modal/pointInfo', ({activeModal}) => {
        return {activeModal: 'pointInfo'};
    });
    store.on('modal/addTeam', ({activeModal}) => {
        return {activeModal: 'addTeam'};
    });

};

