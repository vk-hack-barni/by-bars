import createStore from "storeon";
import connect from '@vkontakte/vk-connect';
import {Lists} from "./lists";
import {Modal} from "./modal";
import {User} from "./users";
import {Stories} from "./stories";

let globalStore = store => {

    // Initial state
    store.on('@init', () => ({
        position: {
            lat: 59.935885,
            long: 30.326021,
            available: 0
        },
        activeStory: 'map',
        mapState: 'all'
    }));

    store.on('init', () => {
        store.dispatch('getToken');
        store.dispatch('getUserInfo');
        store.dispatch('getBars');
        store.dispatch('getPoints');
        store.dispatch('getQuests');
        store.dispatch('getFriendsInfo');
        store.dispatch('getUsers');
        connect.send("VKWebAppGetGeodata", {});
    });

    store.on('onStoryChange', ({activeStory}, newActiveStory) => {
        return {activeStory: newActiveStory};
    });

    store.on('map/setState', ({mapState, selectQuestInfo}, newMapState) => {
        if (!(newMapState === 'quest' && !selectQuestInfo.id)) {
            return {mapState: newMapState};
        } else store.dispatch('popout/emptyQuest');
    });

    store.on('goToQuest', ({mapState, selectQuestInfo}) => {
        return {mapState: 'all', activeStory: 'quest'};
    });

    store.on('setGeo', ({position}, newPosition) => {
        return {position: newPosition};
    });

    connect.subscribe((e) => {
        switch (e.detail.type) {
            case "VKWebAppGetUserInfoResult":
                store.dispatch('setUserInfo', e.detail.data);
                break;
            case "VKWebAppAccessTokenReceived":
                store.dispatch('setToken', e.detail.data.access_token);
                break;
            case "VKWebAppGeodataResult":
                let data = e.detail.data;
                if (data.available) store.dispatch('setGeo', data);
                break;
            case "VKWebAppCallAPIMethodResult":
                console.log(e.detail.data);
                if (e.detail.data.response.count) store.dispatch('setFriends', e.detail.data.response.items);
                else if (e.detail.data.response.upload_url) store.dispatch('stories/upload', e.detail.data.response.upload_url);
                else store.dispatch('setAllUsersInfo', e.detail.data.response);
                break;
            case "VKWebAppCallAPIMethodFailed":
                console.log(e);
                console.log(e.detail.data);
                break;
            default:
                break;
        }
    });
};
export const store = createStore([globalStore, Lists, Modal, User, Stories]);
