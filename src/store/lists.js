export let Lists = store => {

    const apiServer = 'https://bybars-api.basa62.ru';

    // Initial state
    store.on('@init', () => ({
        barList: [],
        pointList: [],
        questList: [],
        selectQuestId: -1,
        selectBarInfo: {},
        selectPointInfo: {},
        selectQuestInfo: {},
        mapBounds: [[0, 0], [0, 0]],
    }));


    //region Bar
    store.on('getBars', async () => {
        let url = apiServer + '/get_all_bar';
        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (newBarList) {
                store.dispatch('setBars', newBarList);
            });
    });

    store.on('setBars', ({barList}, newBarList) => {
        return {barList: newBarList};
    });

    store.on('getBar', async ({selectBarInfo}, id) => {
        store.dispatch('popout/screenSpinner');
        console.log("id: ", id);
        let url = apiServer + '/get_bar/' + id;
        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (newBarInfo) {
                console.log("newBarInfo: ", newBarInfo);
                store.dispatch('setBarInfo', newBarInfo);
                store.dispatch('modal/barInfo');
                store.dispatch('popout/null');
            })
            .catch(error => store.dispatch('popout/error', error));
    });

    store.on('setBarInfo', ({selectBarInfo}, newInfo) => {
        return {selectBarInfo: newInfo};
    });
    //endregion

    //region Point
    store.on('getPoints', async ({pointList}) => {
        let url = apiServer + '/get_all_point';
        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (newBarList) {
                store.dispatch('setPoints', newBarList);
            });
    });

    store.on('setPoints', ({barList}, newBarList) => {
        return {pointList: newBarList};
    });

    store.on('getPoint', async ({selectPointInfo}, id) => {
        store.dispatch('popout/screenSpinner');
        let url = apiServer + '/get_point/' + id;
        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (newPointInfo) {
                store.dispatch('setPoint', newPointInfo);
                store.dispatch('modal/pointInfo');
                store.dispatch('popout/null');
            })
            .catch(error => store.dispatch('popout/error', error));
    });

    store.on('setPoint', ({selectBarInfo}, newInfo) => {
        return {selectPointInfo: newInfo};
    });
    //endregion

    store.on('getQuests', async () => {
        let url = apiServer + '/get_all_quest';
        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (newBarList) {
                store.dispatch('setQuests', newBarList);
            });
    });

    store.on('setQuests', ({questList}, newQuestList) => {
        return {questList: newQuestList};
    });

    store.on('quest/selectQuest', ({selectQuestId}, newSelectQuestId) => {
        return {selectQuestId: newSelectQuestId};
    });

    store.on('quest/null', () => {
        return {selectQuestId: -1, selectQuestInfo: {}, mapState: 'all'};
    });

    store.on('quest/seeMap', () => {
        return {activeStory: 'map', mapState: 'quest'};
    });

    store.on('quest/getQuestInfo', async ({selectQuestId}) => {
        store.dispatch('popout/screenSpinner');
        let url = apiServer + '/get_quest/' + selectQuestId;
        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (newQuestInfo) {
                store.dispatch('quest/setQuestInfo', newQuestInfo);
                store.dispatch('popout/null');
            })
            .catch(error => store.dispatch('popout/error', error));
    });

    store.on('quest/setQuestInfo', ({selectQuestInfo}, newInfo) => {
        return {selectQuestInfo: newInfo};
    });
    store.on('quest/Done', ({selectQuestId, id}) => {
        store.dispatch('popout/Done');
        let url = apiServer + '/add_score/' + id + '/' + 5;
        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then()
            //.catch(error => store.dispatch('popout/error', error));
            .catch(error => console.error(error));
        return {selectQuestId: -1, selectQuestInfo: {}, mapState: 'all'}
    });

    store.on('map/changeBounds', ({mapBounds}, newMapBounds) => {
        if (!(mapBounds[0][0] === newMapBounds[0][0] && mapBounds[1][1] === newMapBounds[1][1])) {
            //TODO запрос на сервер при масштабировании (на хакатоне не используем)
            //console.log(newMapBounds[0][0]+ "/" +newMapBounds[0][1]+ "/" +newMapBounds[1][0]+ "/" +newMapBounds[1][1]);
            //store.dispatch('popout/screenSpinner');
            //store.dispatch('popout/null');
            return ({mapBounds: newMapBounds});
        }
    });
};