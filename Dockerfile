FROM node:11 AS build

RUN mkdir /app && mkdir /src
WORKDIR /src

COPY package.json ./
RUN npm install

COPY . .
RUN npm run build

FROM nginx:alpine
COPY --from=build /src/build /usr/share/nginx/html
