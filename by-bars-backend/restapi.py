import json

from bson import ObjectId
from flask import Flask, Response, request
from pymongo import MongoClient
import requests, random

client = MongoClient('demo8.charlie.vkhackathon.com', username='USER', password='PASS',
                     authSource='admin', authMechanism='SCRAM-SHA-1')

db = client["bybars"]


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


je = JSONEncoder(ensure_ascii=False)

app = Flask(__name__)
app.debug = True


def prepare(data, status: int = 200, mimetype: str = 'application/json'):
    resp = Response(je.encode(data), status=status, mimetype=mimetype)
    # resp.headers.add('Access-Control-Allow-Origin', '*')
    # resp.headers.add('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
    return resp


@app.after_request
def set_headers(resp):
    resp.headers.add('Access-Control-Allow-Origin', '*')
    resp.headers.add('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
    resp.headers.add('Access-Control-Allow-Headers', '*')
    return resp


# id name location
@app.route("/get_all_bar", methods=['GET'])
def get_all_bar():
    try:
        bars = db['bar']
        return prepare([{'id': x['id'], 'name': x['name'], 'location': x['location']} for x in list(bars.find())])
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/get_bar/<id>", methods=['GET'])
def get_bar(id):
    try:
        bars = db['bar']
        return prepare(list(filter(lambda x: x['id'] == id, bars.find()))[0])
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/get_bars/<l1>/<l2>/<p1>/<p2>", methods=['GET'])
def get_bars(l1, l2, p1, p2):
    try:
        bars = db['bar']
        return prepare([{'id': x['id'], 'name': x['name'], 'location': x['location']} for x in list(filter(
            lambda y: (float(l1) < float(y["location"]["lat"]) < float(p1)) and (
                    float(l2) < float(y["location"]["lon"]) < float(p2)), bars.find()))])
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/get_all_point", methods=['GET'])
def get_all_point():
    try:
        points = db['point']
        return prepare([{'id': x['id'], 'name': x['name'], 'location': x['location']} for x in list(points.find())])
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/get_point/<id>", methods=['GET'])
def get_point(id):
    try:
        points = db['point']
        return prepare(list(filter(lambda x: x['id'] == id, points.find()))[0])
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/get_points/<l1>/<l2>/<p1>/<p2>", methods=['GET'])
def get_points(l1, l2, p1, p2):
    try:
        points = db['point']
        return prepare([{'id': x['id'], 'name': x['name'], 'location': x['location']} for x in list(filter(
            lambda y: (float(l1) < float(y["location"]["lat"]) < float(p1)) and (
                    float(l2) < float(y["location"]["lon"]) < float(p2)), points.find()))])
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/get_all_teams", methods=['GET'])
def get_all_team():
    try:
        teams = db['teams']
        return prepare(list(teams.find()))
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/add_team", methods=['POST'])
def add_team():
    try:
        req_json = request.json
        if not req_json:
            return prepare('Not valid JSON!', status=500)
        teams = db['teams']
        req_json['id'] = teams.count()
        teams.insert_one(req_json)
        return prepare({"id": req_json['id']})

    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/get_all_quest", methods=['GET'])
def get_all_quest():
    try:
        quests = db['quest']
        resp = prepare([{'id': x['id'], 'name': x['name'], 'location': x['location'], 'logo': x['logo']} for x in
                        list(quests.find())])
        # resp.headers.add('Access-Control-Allow-Origin', '*')
        return resp
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/get_quest/<id>", methods=['GET'])
def get_quest(id):
    try:
        quests = db['quest']
        L = list(filter(lambda x: x['id'] == int(id), quests.find()))[0]
        for i in range(len(L["route"])):
            rr = requests.get(
                "http://demo8.charlie.vkhackathon.com:8081/get_" + L["route"][i]["type"] + "/" + L["route"][i]["id"])
            L["route"][i]["name"] = rr.json()["name"]
            L["route"][i]["address"] = rr.json()["address"]
            L["route"][i]["location"] = rr.json()["location"]
            L["route"][i]["worktime"] = rr.json()["worktime"]
            L["route"][i]["photo"] = rr.json()["photos"][0]
        resp = prepare(L)
        # resp.headers.add('Access-Control-Allow-Origin', '*')
        return resp
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/get_all_users", methods=['GET'])
def get_all_user():
    try:
        users = db['user']
        return prepare(list(users.find()))
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/add_user", methods=['POST'])
def add_user():
    try:
        req_json = request.json
        if not req_json:
            return prepare('Not valid JSON!', status=500)
        users = db['user']
        # req_json['id'] = users.count()
        if len(list(users.find({"id": req_json["id"]}))) == 0:
            req_json["score"] = random.randint(0, 100)
            users.insert_one(req_json)
        us = list(users.find({"id": req_json["id"]}))[0]
        return prepare(us)

    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/add_score/<id>/<score>", methods=['GET'])
def add_score(id, score):
    try:
        id = int(id)
        score = int(score)
        users = db['user']
        users.find_one_and_update({"id": id}, {"$inc": {"score": score}})
        return prepare(list(users.find({"id": id}))[0])
    except Exception as e:
        return prepare(str(e), status=500)


@app.route("/send_story_image", methods=['POST'])
def send_story_image():
    try:
        req_json = request.json
        print(req_json)
        if not req_json or 'uploadImg' not in req_json or 'uploadUrl' not in req_json:
            return prepare('Not valid JSON!', status=500)
        uploadImg = req_json['uploadImg']
        uploadUrl = req_json['uploadUrl']
        data_ = ('image.png', uploadImg, 'multipart/form-data')
        files = {'file': uploadImg}
        story_resp = requests.post(uploadUrl, files=files)
        return prepare(story_resp.json())
    except Exception as e:
        return prepare(str(e), status=500)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port="8081")
    pass
